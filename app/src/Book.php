<?php

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;

class Book extends Page
{
    private static $db = [
        'PublishDate' => 'Date',
        'Teaser' => 'Text',
        'Author' => 'Varchar',
    ];

    private static $has_one = [
        'CoverImage' => Image::class,
        'Document' => File::class,
    ];

    private static $owns = [
        'CoverImage',
        'Document',
    ];

    private static $can_be_root = false;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', DateField::create('PublishDate', 'Date book was published'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('Author', 'Author of book'), 'Content');
        $fields->addFieldToTab('Root.Main', TextareaField::create('Teaser', 'Summary when previewing book'), 'Content');

        $fields->addFieldToTab('Root.Attachments', UploadField::create('CoverImage'));
        $fields->addFieldToTab('Root.Attachments', $fileField = UploadField::create('Document', 'Actual book file (EPUB only)'));

        $fileField
            ->setFolderName('books')
            ->getValidator()->setAllowedExtensions(['epub']);

        return $fields;
    }
}
