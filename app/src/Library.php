<?php

class Library extends Page
{
    private static $db = [];

    private static $has_one = [];

    private static $allowed_children = [
        Book::class
    ];
}
