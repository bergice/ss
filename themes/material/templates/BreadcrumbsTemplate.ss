<% if $Pages %>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <% loop $Pages %>
                <li class="breadcrumb-item active" aria-current="page">
                    <% if $Last %>
                        $Title.XML
                    <% else %>
                        <a href="$Link">$MenuTitle.XML</a>
                    <% end_if %>
                </li>
            <% end_loop %>
        </ol>
    </nav>
<% end_if %>