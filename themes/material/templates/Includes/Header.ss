<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="$AbsoluteBaseURL">$SiteConfig.Title</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <% loop $Menu(1) %>
                    <li class="nav-item <% if $isCurrent %>active<% end_if %>">
                        <a class="nav-link" href="$Link"
                           title="Go to the $Title page">$MenuTitle <% if $isCurrent %><span
                                class="sr-only">(current)</span><% end_if %></a>
                    </li>
                <% end_loop %>
            </ul>

            <% if $CurrentMember %>
                <a href="admin" target="_blank">
                    <span class="badge">Logged in as $CurrentMember.FirstName</span>
                </a>
            <% end_if %>
        </div>
    </nav>
</header>
