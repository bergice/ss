<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <% base_tag %>
    $MetaTags
</head>
<body>
<% include Header %>
<div class="container-fluid">
    <main class="col-12 py-md-3 bd-content" role="main">
        $Layout
    </main>
</div>
<% include Footer %>
</body>
</html>